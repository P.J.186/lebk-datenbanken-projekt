DROP TABLE IF EXISTS country;
CREATE TABLE country (
       id INTEGER NOT NULL,
       name VARCHAR(100) NOT NULL,
       country_code VARCHAR(3) NOT NULL,
       CONSTRAINT pk_country_id PRIMARY KEY (id)
);

DROP TABLE IF EXISTS city;
CREATE TABLE city (
       id INTEGER NOT NULL,
       name VARCHAR(66) NOT NULL,
       CONSTRAINT pk_city_id PRIMARY KEY (id)
);

DROP TABLE IF EXISTS weather_station;
CREATE TABLE weather_station (
       id INTEGER NOT NULL,
       longitude DECIMAL(10,10) NOT NULL,
       latitude DECIMAL(10,10) NOT NULL,
       city_id INTEGER NOT NULL,
       country_id INTEGER NOT NULL,
       CONSTRAINT pk_weather_station_id PRIMARY KEY (id),
       CONSTRAINT fk_weather_station_to_city FOREIGN KEY (city_id) REFERENCES city(id),
       CONSTRAINT fk_weather_station_to_country FOREIGN KEY (country_id) REFERENCES country(id)
);

DROP TABLE IF EXISTS sensor;
CREATE TABLE sensor (
       id INTEGER NOT NULL,
       type VARCHAR(30) NOT NULL,
       weather_station_id INTEGER NOT NULL,
       CONSTRAINT pk_sensor_id PRIMARY KEY (id),
       CONSTRAINT fk_sensor_to_weather_station FOREIGN KEY (weather_station_id) REFERENCES weather_station(id)
);

DROP TABLE IF EXISTS wind_speed;
CREATE TABLE wind_speed (
       created_at TIMESTAMP NOT NULL,
       mph DECIMAL(10,10),
       sensor_id INTEGER NOT NULL,
       CONSTRAINT pk_wind_speed_created_at_sensor_id PRIMARY KEY (created_at, sensor_id),
       CONSTRAINT fk_wind_speed_to_sensor FOREIGN KEY (sensor_id) REFERENCES sensor(id)
);

DROP TABLE IF EXISTS precipitation;
CREATE TABLE precipitation (
       created_at TIMESTAMP NOT NULL,
       centimetres DECIMAL(10,10),
       sensor_id INTEGER NOT NULL,
       CONSTRAINT pk_precipitation_created_at_sensor_id PRIMARY KEY (created_at, sensor_id),
       CONSTRAINT fk_precipitation_to_sensor FOREIGN KEY (sensor_id) REFERENCES sensor(id)
);

DROP TABLE IF EXISTS humidity;
CREATE TABLE humidity (
       created_at TIMESTAMP NOT NULL,
       percent DECIMAL(10,10),
       sensor_id INTEGER NOT NULL,
       CONSTRAINT pk_humidity_created_at_sensor_id PRIMARY KEY (created_at, sensor_id),
       CONSTRAINT fk_humidity_to_sensor FOREIGN KEY (sensor_id) REFERENCES sensor(id)
);

DROP TABLE IF EXISTS temperature;
CREATE TABLE temperature (
       created_at TIMESTAMP NOT NULL,
       celsius DECIMAL(10,10),
       sensor_id INTEGER NOT NULL,
       CONSTRAINT pk_temperature_created_at_sensor_id PRIMARY KEY (created_at, sensor_id),
       CONSTRAINT fk_temperature_to_sensor FOREIGN KEY (sensor_id) REFERENCES sensor(id)
);
