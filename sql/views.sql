CREATE VIEW current_wind_speed AS
WITH max_created_at AS
(SELECT sensor_id, max(created_at) as created_at
 FROM wind_speed GROUP BY sensor_id)
SELECT ws.created_at, ws.mph, ws.sensor_id FROM wind_speed ws
WHERE ws.created_at = (SELECT max_created_at.created_at
                      FROM max_created_at
                      WHERE max_created_at.sensor_id = ws.sensor_id);

CREATE VIEW current_precipitation AS
WITH max_created_at AS
(SELECT sensor_id, max(created_at) as created_at
 FROM precipitation GROUP BY sensor_id)
SELECT p.created_at, p.centimetres, p.sensor_id FROM precipitation p
WHERE p.created_at = (SELECT max_created_at.created_at
                      FROM max_created_at
                      WHERE max_created_at.sensor_id = p.sensor_id);

CREATE VIEW current_humidity AS
WITH max_created_at AS
(SELECT sensor_id, max(created_at) as created_at
 FROM humidity GROUP BY sensor_id)
SELECT h.created_at, h.percent, h.sensor_id FROM humidity h
WHERE h.created_at = (SELECT max_created_at.created_at
                      FROM max_created_at
                      WHERE max_created_at.sensor_id = h.sensor_id);

CREATE VIEW current_temperature AS
WITH max_created_at AS
(SELECT sensor_id, max(created_at) as created_at
 FROM temperature GROUP BY sensor_id)
SELECT t.created_at, t.celsius, t.sensor_id FROM temperature t
WHERE t.created_at = (SELECT max_created_at.created_at
                      FROM max_created_at
                      WHERE max_created_at.sensor_id = t.sensor_id);

CREATE VIEW all_values AS
SELECT created_at, mph AS value, sensor_id, 'wind_speed' AS type
FROM wind_speed
UNION
SELECT created_at, centimetres AS value, sensor_id, 'precipitation' AS type
FROM precipitation
UNION
SELECT created_at, percent AS value, sensor_id, 'humidity' AS type
FROM humidity
UNION
SELECT created_at, celsius AS value, sensor_id, 'temperature' AS type
FROM temperature;

CREATE VIEW current_values AS
SELECT created_at, mph AS value, sensor_id, 'wind_speed' AS type
FROM current_wind_speed
UNION
SELECT created_at, centimetres AS value, sensor_id, 'precipitation' AS type
FROM current_precipitation
UNION
SELECT created_at, percent AS value, sensor_id, 'humidity' AS type
FROM current_humidity
UNION
SELECT created_at, celsius AS value, sensor_id, 'temperature' AS type
FROM current_temperature;
